package com.example.retrofitlistviewimage;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImageAPI {
    public static final String BASE_URL = "http://10.0.2.2/retrofit/";
    @GET("list_images.php")
    Call<List<ImagesList>> getImages();
}
